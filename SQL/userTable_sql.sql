CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;


CREATE TABLE user (id SERIAL AUTO_INCREMENT, login_id varchar(255) UNIQUE NOT NULL, name varchar(255) NOT NULL, birth_date DATE NOT NULL, password varchar(255) NOT NULL, is_admin boolean DEFAULT false NOT NULL, create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL);
INSERT INTO user (login_id,name,birth_date,password,is_admin) VALUES ('admin','管理者','1999-3-16','password',true);
