package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        System.out.println("UserDetaiServlet内");

        // テスト実施後：ログインしているユーザのインスタンスがsessionスコープに保存されていない場合、userLogin.jspにフォワード
        HttpSession session = request.getSession(); // session定義
        User loginCheck = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック

        if (loginCheck == null) {
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
          dispatcher.forward(request, response);
          return; // ここreturnしなくても行けるのは、何故だ. forwardじゃなくてredirectだからか?
        }


        // userのデータを取得して、requestスコープに記録して、フォワードする
        String id = request.getParameter("id");

        // UserDaoのfindByIdメソッドを呼んで、作成したインスタンスをrequestスコープに保存
        UserDao dao = new UserDao();

        User user = dao.findById(id);
        request.setAttribute("user", user);

        // userDetail.jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDetail.jsp");
        dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
