

package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;


@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserListServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる - - - - - - -

    HttpSession session = request.getSession(); // session定義
    User user = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック


    if (user == null) {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      // ここreturnする必要ないの？
    } // else {以下フォワードまでをここに入れた方がいいかもしれない}

    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO 未実装：検索処理全般
    // TODO 未実装：どんな条件で検索したかわかるように画面に入力値を表示させること
    request.setCharacterEncoding("UTF-8");
    List<User> userList = new ArrayList<User>();
    // //先に定義するんじゃなくて、初期化しないといけない（この状態だとArrayList型で受け渡しているから）
    UserDao userDao = new UserDao();


    String loginId = request.getParameter("login-id");
    String userName = request.getParameter("user-name");
    String startDate = request.getParameter("start-date");
    String endDate = request.getParameter("end-date");
    
    
    //テスト実施後：全て未入力の場合、findAllメソッドを呼ぶ
    if (loginId.equals("") && userName.equals("") && startDate.equals("") && endDate.equals("")) {
      userList = userDao.findAll();
    } else {
      // UserDaoにデータが該当するものを調べるメソッドを作る
      userList = userDao.searchByInput(loginId, userName, startDate, endDate); // 不明なエラー
    }
    

    if (userList == null) {
      request.setAttribute("errMsg", "該当するユーザーはいませんでした");
    }

    request.setAttribute("userList", userList);

    
    // forward先はいずれにせよuserList.jsp
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
    
    
    
    
    
    
    
    
    //
    
  }
}