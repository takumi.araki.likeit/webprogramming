package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // テスト実施後：ログインしているユーザーがいない場合は、userLogin.jspにフォワード
      HttpSession session = request.getSession(); // session定義
      User loginCheck = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック


      if (loginCheck == null) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
        dispatcher.forward(request, response);
        // return;
      }



      // requestスコープにidをセット
      String id = request.getParameter("id"); // Integer.valueOf(String)でintに変換しなくてOK
      request.setAttribute("id", id);

      // findByIdメソッドを呼んで、userのデータをゲット
      UserDao userDao = new UserDao();
      User user = userDao.findById(id);
      request.setAttribute("user", user);

      // userDelete.jspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();

      String id = request.getParameter("user-id");
      String choice = request.getParameter("userDelete");

      if (choice.equals("はい")) {

        int result = userDao.deleteUser(id);
        if (result != 1) {
          System.out.println("データは削除されませんでした");
        } else {
          System.out.println("削除完了");
        }

      } else if (choice.equals("いいえ")) {
        System.out.println("データは削除されません");
      } else {
        System.out.println("エラーが起きました");
      }

      response.sendRedirect("UserListServlet");

	}

}
