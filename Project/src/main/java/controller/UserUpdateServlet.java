package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      System.out.println("UserUpdateServlet、doGet内");

      // テスト実施後：ログインしているユーザのインスタンスがsessionスコープに保存されていない場合、userLogin.jspにフォワード
      HttpSession session = request.getSession(); // session定義
      User loginCheck = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック

      if (loginCheck == null) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
        dispatcher.forward(request, response);
        return; // ここreturnしなくても行けるのは、何故だ. forwardじゃなくてredirectだからか?
      }



      String id = request.getParameter("id");
      // request.setAttribute("id", id);
      System.out.println("doGet内 id = " + id);

      // loginIDを取得するようなメソッドを、UserDaoにつくって、requestスコープにパラメターをセットしないと
      // userUpdateにフォワードした時に、データを表示できない

      // findbyloginidまたはfindAllで取得できないか、新しいメソッドが必要か考える
      // 何がしたいかというと、idの値だけで、1ユーザーのデータを取得したい -> 新しいメソッド、絶対にいる

      
      // findByIdの引数の数を変えたので、同じ処理
      UserDao userDao = new UserDao();
      User user = userDao.findById(id);

      request.setAttribute("user", user);


      // userUpdate.jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      System.out.println("UserUpdateServlet、doPost内");
      request.setCharacterEncoding("UTF-8"); // リクエストパラメータの文字化け防止


      UserDao userDao = new UserDao();

      String id = request.getParameter("id"); // idの受け渡しができていないので、変えてみた
      // //jspファイルに、idという名のパラメターがなかった
      System.out.println("doPost内 id = " + id);
      String loginId = request.getParameter("user-id"); // 要チェック
      System.out.println("UserUpdateServlet、doPost内、ログインID: " + loginId);
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");

      request.setAttribute("id", id);
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", userName);
      request.setAttribute("birthDate", birthDate);


      // 入力に問題があるか、チェック - - - - - - - - - - - - -
      // パスワード一致するか確認
      // ログインID同じものがないか確認
      // 未入力のものがないか確認
      int check = 1; // 1=true,0=false

      // パスワードが一致しない場合確認
      if (!(password.equals(passwordConfirm))) {
        check = 0;
        System.out.println("パスワードが異なる");

        request.setAttribute("errMsgPassword", "パスワードが異なります");
      }

      // 未入力のものがないか確認 //nullと""は別物
      if ((userName).equals("") || (birthDate).equals("")) { // loginIdがnullになることはない、（入力できないから、チェックする必要はない）
        check = 0;
        System.out.println("未入力のものがある");
        request.setAttribute("errMsgNonFill", "未入力のものがあります");

      }

      // 問題がある場合、userUpdate.jspにフォワード
      if (check == 0) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return; // ここreturnするの忘れないようにね
      }
      // 入力に問題があるか、チェック - - - - - - - - - - - - - -
      
      
      // パスワードの暗号化
      PasswordEncorder passwordEncorder = new PasswordEncorder();
      String encodePassword = passwordEncorder.encordPassword(password);


      // UserDao内にアップデート機能を持つメソッドを追加し、resultをInt型で戻す
      int result = userDao.updateUser(id, userName, encodePassword, birthDate);

      if (result != 1) {
        System.out.println("アップデート失敗");
        request.setAttribute("errMsgUpdate", "アップデートに失敗しました");

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }


      response.sendRedirect("UserListServlet");



	}

}
