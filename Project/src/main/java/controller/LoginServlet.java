
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public LoginServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる - - - - - - -
    // doPostを見たところ、sessionに記録されているのはuserのインスタンスなので、そこがnullかどうか調べる方向で今度は行く


    HttpSession session = request.getSession(); // session定義
    User user = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック
    

    if (user != null) {
      response.sendRedirect("UserListServlet"); // ログイン情報が保存されているので、UserListServletへリダイレクト
      // return;
    } else {
    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8"); // リクエストパラメータの文字化け防止

    // リクエストパラメータからデータ取得
    // ここはrequestパラメターでかかれているので、sessionパラメターに引っかからないのは当然!
    String loginId = request.getParameter("loginid");
    String password = request.getParameter("password");

    // パスワードの暗号化
    PasswordEncorder passwordEncorder = new PasswordEncorder();
    String encodePassword = passwordEncorder.encordPassword(password);

    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    UserDao userDao = new UserDao();
    User user = userDao.findByLoginInfo(loginId, encodePassword); // このインスタンスuserには、ログインIDとパスワードの情報しか入っていない（idは分からない）

    /** テーブルに該当のデータが見つからなかった場合 * */
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
      // 入力したログインIDを画面に表示するために値をセット
      request.setAttribute("loginId", loginId); // requestスコープ内のloginIdというkeyにloginIdの値を代入してforwardしますよ
      // request.setAttribute("password", password); // テスト実施後：ログインに失敗したら表示してはいけないそう

      // ログインjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /** テーブルに該当のデータが見つかった場合 * */
    // セッションにユーザの情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user); // この時に、idの値が正しく受け渡されていない可能性がある（常に0になってしまう）
    // 現段階で、userに入っているデータは、id,login_id,passwordの3つのはず

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }
}
