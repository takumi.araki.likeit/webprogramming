
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;



/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // response.getWriter().append("Served at: ").append(request.getContextPath());

        // ユーザーを登録するためにデータの入力を行うため、userAdd.jspにフォワードしたい


        // - - - - - - -いまここをいじっている
        // テスト実施後：ログインしているユーザーがいない場合は、userLogin.jspにフォワード
        HttpSession session = request.getSession(); // session定義
        User user = (User) session.getAttribute("userInfo"); // "userInfo"という名でsessionスコープに今ログインしてる人がいるかチェック


        if (user == null) {
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
          dispatcher.forward(request, response);
          // return;
        }
        // - - - - -


        // ひとまずフォワードする処理だけ行いたい
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);


      }




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // doGet(request, response); 消します

        request.setCharacterEncoding("UTF-8"); // 文字化け防止
        UserDao dao = new UserDao();
        // HttpSession session = request.getSession();


        // ユーザーを登録する処理を行う
        // requestスコープでいいので、getParameterを使って、データを取得する
        // その後、データを追加したいので、インスタンスを作り、コンストラクターまたはゲッターでデータを登録する
        // 同じログインIDが既に登録されている場合はもう一度、入力してもらいたいので、もう一度（errMsgと共に）userAdd.jspに飛ぶ
        // UserDaoの中に、ユーザーを追加するメソッドを作らなきゃだめだ


        String loginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("password-confirm");
        String userName = request.getParameter("user-name");
        String birthdate = request.getParameter("birth-date");


        // パラメター取得できてるかチェック
        System.out.println("LoginID: " + loginId);
        System.out.println("Password: " + password);
        System.out.println("PasswordConfirm: " + passwordConfirm);
        System.out.println("userName: " + userName);
        System.out.println("Birthdate: " + birthdate);


        // requestスコープがうまくいかないので、最悪sessionスコープで代用する -> requestでうまくいきました
        request.setAttribute("loginId", loginId);
        // request.setAttribute("password", password); //テスト実施後：パスワードは、requestスコープに保存しなくてOK
        request.setAttribute("userName", userName);
        request.setAttribute("birthdate", birthdate);


        // パスワード一致するか確認
        // ログインID同じものがないか確認
        // 未入力のものがないか確認
        int check = 1; // 1=true,0=false //booleanにした方がいい


        // パスワードが一致するか確認
        if (!password.equals(passwordConfirm)) { // if (password != passwordConfirm) { //カッコなくてもいける
          check = 0;
          System.out.println("パスワードが異なる");

          request.setAttribute("errMsgPassword", "パスワードが異なります");
        }

        // ログインID同じものがないか確認
        // に新しいメソッドを追加（同じログインIDがあるかどうか確認する用）
        // 説明付け加える、booleanを返すメソッドを作る（daoではなく、utilなどにつくる）この下に、メソッドつくるのもあり
        String loginIdCheck = dao.findLoginId(loginId); // UserDaoのログインIDが既にあるか確認するメソッドを呼ぶ
                                                           // //メソッド名は動詞から始める

        if (loginIdCheck != null) {
          check = 0;
          System.out.println("同じログインIDの人がいる");
          request.setAttribute("errMsgPassword2", "同じログインIDの人がいます");

        }

        // 未入力のものがないか確認 //nullと""は別物
        if ((loginId).equals("") || (password).equals("") || (passwordConfirm).equals("")
            || (userName).equals("") || (birthdate).equals("")) {
          check = 0;
          System.out.println("未入力のものがある");
          request.setAttribute("errMsgPassword3", "未入力のものがあります");

        }

        // 問題がある場合、userAdd.jspにフォワード
        if (check == 0) {
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
          dispatcher.forward(request, response);
          return;
        }


        // パスワードの暗号化
        PasswordEncorder passwordEncorder = new PasswordEncorder();
        String encodePassword = passwordEncorder.encordPassword(password);

        dao.addUser(loginId, encodePassword, userName, birthdate);// birthdateSql);
        // 大事ここ、リダイレクトしましょう
        response.sendRedirect("UserListServlet"); // ここ超大事 最後リダイレクト（またはフォワード）しないとどこにも行かない



	}

}
