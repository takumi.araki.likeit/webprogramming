package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */

  // ログインIDとパスワードが一致するユーザを取得
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 該当するものがない場合、nullを返す
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id"); // idも受け渡すことで、UserListを表示する時に、今ログインしている人のidと照らし合わせて更新できるか変えたい
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      boolean isAdmin = rs.getBoolean("is_admin");
      System.out.println("UserDao内isAdmin = " + isAdmin); // ここでtrueと出力されるということは、isAdminにはいってるのはboolean型のtrueという値（DBでは0や1かもしれないが）
      return new User(id, loginIdData, nameData, isAdmin); // ここ //ここでidも取得してはどうか（常に0にセットされるのを防ぐために）
                                                           // ここでbooleanの値も取得してはどうか

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // UserDetailServletとUserUpdateServletとUserDeleteServlet用
  public User findById(String id) { // 引数はintで定義した方が、賢い可能性あり（SQLはString型でデータをセットされても定義されたデータ型に自動で変換するようだ）（だが同じ型(int)の方が賢明）
    Connection conn = null;
    PreparedStatement pStmt = null;


    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();



      // 該当するものがない場合、nullを返す
      if (rs.next()) {
        // 必要なデータのみインスタンスのフィールドに追加
        int idInt = rs.getInt("id");// = Integer.valueOf(id);
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        // user型のインスタンスを作成し、リターン
        // return new User(loginId, password, name, birthDate, createDate, updateDate);
        // //passwordとuserNameの順番注意（引数6の時）
        return new User(idInt, loginId, name, birthDate, password, isAdmin, createDate, updateDate); // 引数8で全然できちゃうのでOK

      } else {
        return null;
      }
      

    } catch (SQLException e) {
      System.out.println("SQLExcepitonが投げられました");

      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      
        try {
          if (conn != null) {
          conn.close();
          }
          if (pStmt != null) {
            pStmt.close();
          }
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    
  }

  // 全てのユーザ情報を取得する
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加

      // rs.next(); // id=0の管理者の情報はfindAllでは載せたくないので、一つ飛ばす。たぶんこれでOK
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        if (!isAdmin) {
          userList.add(user); // 管理者のデータは出力しない
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  // ユーザーを新規登録する（引数4）
  public void addUser(String loginId, String password, String userName, String birthdate) { // 一旦Stringのままやってみる
    Connection conn = null;
    PreparedStatement pStmt = null;

    System.out.println("addUser内");
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // INSERT文を準備
      String sql = "INSERT INTO user (login_id,name,birth_date,password) VALUES (?,?,?,?)";

      // INSERTを実行し、値を代入
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthdate);
      pStmt.setString(4, password);
      int result = pStmt.executeUpdate(); // ここは、引数いらない

      System.out.println("result = " + result);
      /*
       * if (result != 1) { // ここあってる? System.out.println("result = " + result); throw new
       * SQLException(); // ここ大丈夫? }
       */

      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
        return;
    } finally {
      // データベース切断
      try {
        if (conn != null) {
          conn.close();
        }
        if (pStmt != null) {
          pStmt.close();
        }

      } catch (SQLException e) {
          e.printStackTrace();
          return;
      }
    }
  }

  // 同ログインIDがあるかどうか確認する
  public String findLoginId(String loginId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECTを実行し、結果表を取得
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 該当するものがない場合、nullを返す
      if (rs.next()) {
        String loginIdData = rs.getString("login_id");
        return loginIdData;
      } else {
        return null;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断

        try {
          if (conn != null) {
            conn.close();
          }
          if (pStmt != null) {
            pStmt.close();
          }
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }

    }
  }

  // 検索条件にヒットするuserがいるか確認する
  // ログインIDは完全一致
  // ユーザ名は部分一致
  // 生年月日は、開始日〜終了日の間
  public List<User> searchByInput(String loginId, String userName, String startDate, // List<User>
      String endDate) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    List<User> userList = new ArrayList<User>();

    System.out.println("LoginId: " + loginId + ", userName: " + userName + ", startDate: "
        + startDate + ", endDate: " + endDate);


    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT * FROM user WHERE is_admin = false AND "; // ここでアドミンを弾いちゃう
      String sql2 = "login_id = ?";
      String sql3 = "name LIKE CONCAT('%',?,'%')";
      String sql4 = "birth_date BETWEEN ? AND ?";
      
      String sqlAnd = " AND ";
      
      boolean thereIsLoginId = false;
      boolean thereIsName = false;
      boolean thereIsBirthDate = false;
      

      StringBuilder sb = new StringBuilder();
      sb.append(sql);

      if (!(loginId.equals(""))) {
        sb.append(sql2);
        thereIsLoginId = true;
      }      

      if (!(userName.equals(""))) {
        if (thereIsLoginId) {
          sb.append(sqlAnd);
        }
        sb.append(sql3);
        thereIsName = true;
      }
      
      if (!(startDate.equals("")) && !(endDate.equals(""))) {
        if (thereIsLoginId||thereIsName) {
          sb.append(sqlAnd);
        }
        sb.append(sql4);
        thereIsBirthDate = true;
      }
            
      
      System.out.println("thereIsLoginId: " + thereIsLoginId + ", thereIsName: " + thereIsName
          + ", thereIsBirthDate: " + thereIsBirthDate);



      String sqlBuild = sb.toString();
      // ここまではOK ----------------------------------------------

      // PreparedStatementにSQL文をセット
      pStmt = conn.prepareStatement(sqlBuild);
      
      
      // パラメターをセット // ここの処理はできている
      // 最初のパラメターをセット
      if (thereIsLoginId) {
        pStmt.setString(1, loginId);
        thereIsLoginId = false;
      } else if (thereIsName) {
        pStmt.setString(1, userName);
        thereIsName = false;
      } else if (thereIsBirthDate) {
        pStmt.setString(1, startDate);
        pStmt.setString(2, endDate);
        thereIsBirthDate = false;
      } else {
        return null;
      }

      // 2つ目以降のパラメターをセット
      if (thereIsName) {
        pStmt.setString(2, userName);
        thereIsName = false;
      } else if (thereIsBirthDate) {
        pStmt.setString(2, startDate);
        pStmt.setString(3, endDate);
        thereIsBirthDate = false;
      }

      // 最後のパラメターをセット
      if (thereIsBirthDate) {
        pStmt.setString(3, startDate);
        pStmt.setString(4, endDate);
        thereIsBirthDate = false;
      }

      // pStmt.setString(1, loginId);
      // pStmt.setString(2, userName);
      // pStmt.setString(3, startDate);
      // pStmt.setString(4, endDate);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {

        do {
          // 必要なデータのみインスタンスのフィールドに追加
          int id = rs.getInt("id");
          String loginIdData = rs.getString("login_id");
          String name = rs.getString("name");
          Date birthDate = rs.getDate("birth_date");
          String password = rs.getString("password");
          boolean isAdmin = rs.getBoolean("is_admin");
          Timestamp createDate = rs.getTimestamp("create_date");
          Timestamp updateDate = rs.getTimestamp("update_date");

          User user =
              new User(id, loginIdData, name, birthDate, password, isAdmin, createDate, updateDate);
          // return user;

          if (!isAdmin) { // ここで管理者の情報を出力してしまうのはまずいので
            userList.add(user);
          } /*
             * else { if () return null; // テスト実施後：ついでにnullをreturnしておけば、エラーメッセージも出力することができる //
             * //これだと、adminの生年月日が含まれた時点でnullがreturnされてしまう }
             */

        } while (rs.next());

    } else {
      return null;
    }

    /*
     * else { // 該当するものがない場合、nullを返す System.out.println("該当する人はいなかった模様"); return null; }
     */

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
        try {
          if (conn != null) {
            conn.close();
          }
          if (pStmt != null) {
            pStmt.close();
          }
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
      return userList;
      // return null;


  }

  // 更新機能を追加する
  public int updateUser(String id, String userName, String password, String birthDate) {

    System.out.println("updateUser内、id= " + id);
    Connection conn = null;
    PreparedStatement pStmt = null;
    int result = 0;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = null;
      if (password.equals("")) {
        sql = "UPDATE user SET name=?,birth_date=? WHERE id = ?";

        // UPDATEを実行し、パラメターをセット
        pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userName);
        pStmt.setString(2, birthDate);
        pStmt.setString(3, id);
        result = pStmt.executeUpdate();
      } else {
        sql = "UPDATE user SET name=?,birth_date=?,password=?  WHERE id = ?";

        // UPDATEを実行し、パラメターをセット
        pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userName);
        pStmt.setString(2, birthDate);
        pStmt.setString(3, password);
        pStmt.setString(4, id);
        result = pStmt.executeUpdate();
      }



      System.out.println("result = " + result);
      return result;

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断

      try {
        if (conn != null) {
          conn.close();
        }
        if (pStmt != null) {
          pStmt.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }

    }
    return result;
  }

  // ユーザー削除用
  public int deleteUser(String id) {

    System.out.println("deleteUser内、id= " + id);
    Connection conn = null;
    PreparedStatement pStmt = null;
    int result = 0;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE id = ?";

      // UPDATEを実行し、パラメターをセット
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      result = pStmt.executeUpdate();


      System.out.println("result = " + result);
      return result;


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断

      try {
        if (conn != null) {
          conn.close();
        }
        if (pStmt != null) {
          pStmt.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }

    }
    return result;
  }

}
