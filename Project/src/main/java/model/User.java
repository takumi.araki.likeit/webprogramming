package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/*
 * Userテーブルのデータを格納するためのBeans
 */
public class User implements Serializable { // BeansなのでSerializableを実装
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  private Timestamp createDate;
  private Timestamp updateDate;

  // デフォルトコンストラクター
  public User() {}

  // コンストラクターは、引数8の全データをセットするものと引数6の新規登録用のものがあれば十分だが、書いてしまったのでよしとする

  // コンストラクター追加（id,ログインID,ログイン名,isAdmin）（ログイン用+id&isAdmin取得用）
  public User(int id, String loginId, String name, boolean isAdmin) {
    System.out.println("Userコンストラクタ内isAdmin = " + this.isAdmin);
    System.out.println("=======コンストラクター引数4=======");
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.isAdmin = isAdmin;
  }

  // コンストラクター追加（detail表示・update・delete用）//今はupdateに取り組んでいる
  public User(String loginId, String userName, Date birthdate, //int id, String password,
      Timestamp createDate,
      Timestamp updateDate) {
    System.out.println("=======コンストラクター引数5=======");
    //this.id = id;
    this.loginId = loginId;
    this.name = userName;
    //this.password = password;
    this.birthDate = birthdate;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  // コンストラクター追加（詳細表示用）（引数3）
  public User(String loginId, String userName, Date birthdate) {
    System.out.println("=======コンストラクター引数3=======");
    this.loginId = loginId;
    this.name = userName;
    this.birthDate = birthdate;
  }

  // コンストラクター追加（新規登録用）
  public User(String loginId, String password, String userName, Date birthdate,
      Timestamp createDate, Timestamp updateDate) {
    System.out.println("=======コンストラクター引数6=======");
    this.loginId = loginId;
    this.password = password;
    this.name = userName;
    this.birthDate = birthdate;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  // コンストラクター（全データセット）
  public User(int id, String loginId, String name, Date birthDate, String password, boolean isAdimn,
      Timestamp createDate, Timestamp updateDate) {
    // System.out.println("=======コンストラクター引数8(全部)=======");
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdimn;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  // アクセサ
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean getIsAdmin() { // もともとisAdminだったものをgetIsAdminに変更
    return isAdmin;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }
}